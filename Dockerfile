ARG VERSION=14
FROM postgres:${VERSION} as postgres

# Install pgtap
RUN apt update \
    && apt install -y --no-install-recommends \
        ca-certificates \
        gcc \
        libcurl4-openssl-dev \
        libpq-dev \
        make \
        patch \
        postgresql-server-dev-$PG_MAJOR \
        unzip \
    && rm -rf /var/lib/apt/lists/*

ADD https://github.com/theory/pgtap/archive/master.zip .
RUN unzip master.zip && cd pgtap-master && make install

#
# Slim version minus the zip file
#
FROM postgres:${VERSION}

# Copy pgtap
ARG VERSION
COPY --from=postgres /usr/share/postgresql/${VERSION}/extension/pgtap* /usr/share/postgresql/${VERSION}/extension/
