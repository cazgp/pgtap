# pgtap

This is a repo around pgtap the database testing tool.

## Build

```
podman build -t cazgp/pgtap[:VERSION] [--build-arg VERSION=<pg_version>] .
podman build -t cazgp/pgtap:14 --build-arg VERSION=14 .
```

## Run

```
podman run -it --rm cazgp/pgtap
```

## Push

```
podman login
podman push cazgp/pgtap[:VERSION]
```
